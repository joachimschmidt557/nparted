/*
    nparted - a frontend to libparted for manipulating disk partitions
    thinked to dbootstrap
    Copyright (C) 1998-2000 Mario Teijeiro Otero Esteve Fern�ndez 
    Jaime Villate La Espiral
	
    	Mario Teijeiro Otero <asimovi@teleline.es>
	Esteve Fern�ndez < esteve@crosswinds.net >
	Jaime Villate <villate@fe.up.pt>
	La Espiral  http://www.laespiral.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <nparted.h>
#include <ctype.h>
void i18n_initialize (void)
{
	/* Si HAVE_NLS es definido, asumimos la existencia de 
	 * tres funciones invocadas aqu� */
#ifdef HAVE_NLS
	/* Ponemos la localizaci�n actual */
	setlocale(LC_MESSAGES,"");
	bindtextdomain(PACKAGE,LOCALDIR);
	textdomain(PACKAGE);	
#endif 
}

static char *device_type[]={"UNKNOWN","SCSI","IDE","DAC960","CPQARRAY"};

char *get_name_device_type ( PedDeviceType type){
	switch (type){
		case PED_DEVICE_UNKNOWN: return device_type[0];
		case PED_DEVICE_SCSI:return device_type[1];
		case PED_DEVICE_IDE:return device_type[2];
		case PED_DEVICE_DAC960:return device_type[3];
		case PED_DEVICE_CPQARRAY: return device_type[4];

	}
	return device_type[0];
}
/* FIXME: Mejor que salir directamente, llamar a las funciones
 * para que cierren todos los dispositivos abiertos y esass cosas*/
void * NP_malloc(int size){
	void *p;

	p=malloc(size);
	if(p==NULL){	
		ped_exception_throw(PED_EXCEPTION_FATAL, 
			PED_EXCEPTION_OK,_("I can't malloc, abort")); 
			ped_exception_catch(); 
		exit(-1); 
	}
	return p;
}

char* select_file_system ( void )
{
	newtComponent list,form;
	struct newtExitStruct toexit;
	PedFileSystemType* fs_type;
	char *type;
	int i;


	list=newtListbox(5,1,5,NEWT_FLAG_SCROLL|NEWT_FLAG_RETURNEXIT);
	newtListboxSetWidth(list,20);
	fs_type=NULL;
	i=0;
	while( (fs_type=ped_file_system_type_get_next (fs_type)) !=NULL){
		if (!fs_type->name) break;
		newtListboxAppendEntry(list,fs_type->name, fs_type->name);
		i++;
	}
	newtCenteredWindow(30, 7,_("Please, select file system"));
	form=newtForm(NULL,NULL,0);
	newtFormAddComponents(form,list,NULL);
	newtFormAddHotKey(form,(int)'');
	newtFormRun(form,&toexit);

	if (toexit.reason==NEWT_EXIT_COMPONENT)
		type=(char*)newtListboxGetCurrent(list);  
	else
		type=NULL;	
	newtFormDestroy(form);
	newtPopWindow();
	
	return type;
}

char select_partition_type (void)
{
	newtComponent list,form;
	struct newtExitStruct toexit;
	char types[]={'p','l','e'};
	char *type;
	int i;


	list=newtListbox(5,1,5,NEWT_FLAG_SCROLL|NEWT_FLAG_RETURNEXIT);
	newtListboxSetWidth(list,20);
	i=0;
	newtListboxAppendEntry(list,_("primary"), &types[0]);
	newtListboxAppendEntry(list,_("logical"), &types[1]);
	newtListboxAppendEntry(list,_("extended"), &types[2]);

	newtCenteredWindow(30, 7,_("Please, select partition_type"));
	form=newtForm(NULL,NULL,0);
	newtFormAddComponents(form,list,NULL);
	newtFormAddHotKey(form,(int)'');
	newtFormRun(form,&toexit);

	if (toexit.reason==NEWT_EXIT_COMPONENT){
		type=(char*)newtListboxGetCurrent(list);  
		newtFormDestroy(form);
		newtPopWindow();
		return *type;
	}
	newtFormDestroy(form);
	newtPopWindow();
	return '\0';
}

PedSector is_sector_valid(char *d){
	char *p;
	p=d;
	while (*p!='\0' && (isdigit(*p)||isspace(*p)) ) p++;
	if (*p=='\0'){
		return strtol(d,&p,10);
	}else{
		return -1;
	}
}
