/*
 *
    nparted - a frontend to libparted for manipulating disk partitions
    thought to dbootstrap
    Copyright (C) 1998-2000 Mario Teijeiro Otero Esteve Fern�ndez 
    Jaime Villate La Espiral
	
    	Mario Teijeiro Otero <asimovi@teleline.es>
	Esteve Fern�ndez < esteve@crosswinds.net >
	Jaime Villate <villate@fe.up.pt>
	La Espiral  http://www.laespiral.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Codigo a realizar cuando se edita una partici�n */
#include "nparted.h"
/* Joder que funci�n tan grande, me parece que hay que particionarla
 * valga la redundancia */
typedef struct _edit edit;
struct _edit{
	newtComponent form,ok,cancel,grow,max,now,start,end,tamano,
		start_now, start_min, end_now, end_max, tamano_now,
		tamano_max, new,start_new,end_new,tamano_new,
		sel_file_system, file_system,progress;
	struct newtExitStruct toexit;
};

int init_form(edit *form,char *title){
	newtCenteredWindow(mainwin_size.width,mainwin_size.height,title);

	form->form=newtForm(NULL,NULL,0);

	form->start=		newtLabel( 1, 3,_("Start")  );
	form->end=		newtLabel( 1, 5,_("End")    );
	form->tamano=		newtLabel( 1, 7,_("Tamano") );

	form->now=		newtLabel(14, 1,_("Now")    );
	form->start_now=	newtLabel(12, 3,"");
	form->end_now=	newtLabel(12, 5,"");
	form->tamano_now=	newtLabel(12, 7,"");

	form->max=		newtLabel(34, 1,_("Max."));
	form->start_min=	newtLabel(32, 3,"");
	form->end_max=	newtLabel(32, 5,"");
	form->tamano_max=	newtLabel(32, 7,"");

	form->progress=	newtScale(2, 14, mainwin_size.width-5,100);

	form->new=		newtLabel(52, 1,_("New geom"));
	form->start_new=	newtEntry(52, 3,NULL,10,NULL,NEWT_ENTRY_SCROLL
				|NEWT_FLAG_RETURNEXIT);
	form->end_new=	newtEntry(52, 5,NULL,10,NULL,NEWT_ENTRY_SCROLL
				|NEWT_FLAG_RETURNEXIT);

	form->tamano_new=	newtEntry(52, 7,NULL,10,NULL,NEWT_ENTRY_SCROLL
				|NEWT_FLAG_RETURNEXIT);


	form->grow=newtCheckbox(5,10,_("Grow too much posibble"),' ',NULL,NULL);
	form->file_system=newtLabel(50,10,_("Without Filesystem"));
	form->sel_file_system=newtButton(38,9,_("Select"));

	form->ok=newtButton(10,mainwin_size.height-4,_("OK"));
	form->cancel=newtButton(mainwin_size.width-20,mainwin_size.height-4,_("CANCEL"));
		
	newtFormAddComponents(form->form,
		form->now,form->max,form->start,form->start_now,form->start_min,
		form->end,form->end_now,form->end_max,
		form->tamano,form->tamano_now,form->tamano_max,
		form->grow, form->ok,form->cancel,
		form->new,form->start_new,form->end_new,form->tamano_new,
		form->sel_file_system,form->file_system,form->progress,NULL);
	newtFormAddHotKey(form->form,NEWT_KEY_LEFT);
	newtFormAddHotKey(form->form,NEWT_KEY_RIGHT);
	newtFormAddHotKey(form->form,NEWT_KEY_PGDN);
	newtFormAddHotKey(form->form,NEWT_KEY_PGUP);
	newtFormAddHotKey(form->form,(int)'');
	/* Al parecer no se puede a�adir keys a componentes, solo
	   a forms, si no da segment fault */
	//	newtFormAddHotKey(form->grow,(int)' ');
	return 1;

}

typedef struct _disk_info disk_info;
struct _disk_info{
	PedDevice *dev;
	PedDisk *disk;
	PedPartition *part;
	PedGeometry* geom; /* Max geometry to grow*/
	PedFileSystemType * fs_type;
	PedConstraint* constraint;
	int fs_name_changed;
};


int init_disk_info(disk_info *disk,PedDevice *dev,int npart)
{
	if (dev==NULL){ 
		ped_exception_throw(PED_EXCEPTION_ERROR,
				PED_EXCEPTION_OK,
				_("Device not valid"));
		return 0;
	}
	disk->dev=dev;
	disk->disk=ped_disk_open(disk->dev);
	disk->constraint=ped_constraint_any(disk->disk);
	disk->part=ped_disk_get_partition(disk->disk,npart);
	disk->fs_name_changed=0;

	if (disk->disk==NULL) goto error; 
	if (disk->part==NULL){
		ped_exception_throw(PED_EXCEPTION_ERROR,
			PED_EXCEPTION_OK,
			_("Partition not valid"));
		goto error_disk_close;
	}
	disk->fs_type=ped_file_system_probe(&(disk->part->geom));
	disk->geom=ped_disk_get_max_partition_geometry(disk->disk, disk->part,disk->constraint);
	if (disk->geom==NULL){
		ped_exception_throw(PED_EXCEPTION_ERROR,
			PED_EXCEPTION_OK,
			_("I can't get de max partition's geometry"));
		goto error_disk_close;
	}

	return 1;

error_disk_close: 
	ped_disk_close(disk->disk);
error:
	return 0;

}


int fill_form(edit *form, disk_info *disk,strech *new_size)
{
	if (disk->fs_type!=NULL && !disk->fs_name_changed){
		newtLabelSetText(form->file_system,disk->fs_type->name);	
	}

	sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,disk->part->geom.start));
	newtLabelSetText(form->start_now,ptrb);

	sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,disk->part->geom.end));
	newtLabelSetText(form->end_now,ptrb);

	sprintf(ptrb,"%d Mb", (int)SEC2MB((disk->part->geom.end-disk->part->geom.start+1)));
	newtLabelSetText(form->tamano_now,ptrb);

	sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,disk->geom->start));
	newtLabelSetText(form->start_min,ptrb);
	sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,disk->geom->end));
	newtLabelSetText(form->end_max,ptrb);
	sprintf(ptrb,"%d Mb", (int)SEC2MB((disk->geom->end-disk->geom->start+1)));
	newtLabelSetText(form->tamano_max,ptrb);

	newtScaleSet(form->progress,(100* (new_size->end))/disk->geom->end);
	return 0;
}

int run_form(edit *form, disk_info *disk, int npart)
{
	newtComponent answer;
	char fs_name[256];
	strech new_size;
	int change_geom=0;
	long tam;
	int key;

	new_size.start=disk->part->geom.start;
	new_size.end=disk->part->geom.end;

	answer=NULL;

	strcpy(fs_name,"Without Filesystem");
	do{
		fill_form(form, disk,&new_size);
		newtFormRun(form->form,&(form->toexit));
		key=-1;
		if(form->toexit.reason==NEWT_EXIT_COMPONENT)
			answer=form->toexit.u.co;
		else
			key=form->toexit.u.key;
		
		if (key==(int)' '){
			new_size.end=disk->geom->end;
			new_size.start=disk->geom->start;
			
			if(new_size.end<disk->geom->start) new_size.end=disk->geom->start;

			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.end));
			newtEntrySet(form->end_new,ptrb,1);
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.start));
			newtEntrySet(form->start_new,ptrb,1);
			sprintf(ptrb,"%d", (int)SEC2MB( (new_size.end- new_size.start)));
			newtEntrySet(form->tamano_new,ptrb,1);
		}
		if(answer==form->sel_file_system){
			char *aux;
			if(ped_exception_throw(PED_EXCEPTION_INFORMATION,
				PED_EXCEPTION_OK|PED_EXCEPTION_CANCEL,
				_("Change the file system will destroy any data")
				)==PED_EXCEPTION_OK){
				aux=select_file_system();	
				if(aux!=NULL){
					strcpy(fs_name,aux);
					newtLabelSetText(form->file_system,fs_name);
					disk->fs_name_changed=1;
				}
			}
			ped_exception_catch();
		}
		if(answer==form->start_new||answer==form->end_new){
			change_geom=1;
			new_size.start=is_sector_valid(newtEntryGetValue(form->start_new));
			new_size.end=is_sector_valid(newtEntryGetValue(form->end_new));
			 if (new_size.start==-1||new_size.end==-1){
                                ped_exception_throw(PED_EXCEPTION_ERROR,
                                              PED_EXCEPTION_OK,
                                          _("a number is needed"));
                                ped_exception_catch();
				newtEntrySet(form->end_new,"",1);
				newtEntrySet(form->start_new,"",1);
				newtEntrySet(form->tamano_new,"",1);
			}else{
				new_size.start=CIL2SEC(disk->dev,new_size.start);
				if(new_size.start<64) new_size.start=64;
				new_size.end=CIL2SEC(disk->dev,new_size.end+1)-1;
				sprintf(ptrb,"%d", (int)SEC2MB( (new_size.end-
							    new_size.start)));
				newtEntrySet(form->tamano_new,ptrb,1);
			}
		}
		if (answer==form->tamano_new){
			change_geom=1;
			new_size.start=is_sector_valid(newtEntryGetValue(form->start_new));
			tam=is_sector_valid(newtEntryGetValue(form->tamano_new));
			if(new_size.start==-1||tam==-1){
                                ped_exception_throw(PED_EXCEPTION_ERROR,
                                              PED_EXCEPTION_OK,
                                          _("a number is needed"));
                                ped_exception_catch();
			}else{
				if(new_size.start==0) new_size.start=disk->part->geom.start;
				new_size.end=MB2SEC((tam))+new_size.start;
				sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.end));
				newtEntrySet(form->end_new,ptrb,1);
				sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.start));
				newtEntrySet(form->start_new,ptrb,1);
			}
		}
		if(answer==form->ok){
			if (newtCheckboxGetValue(form->grow)=='*'){
				new_size.start=disk->geom->start;
				new_size.end=disk->geom->end;
				change_geom=1;
			}
			if(ped_exception_throw(PED_EXCEPTION_INFORMATION,
				PED_EXCEPTION_OK|PED_EXCEPTION_CANCEL,
				_("Are you soure?")
				)==PED_EXCEPTION_OK)
				if(change_geom){
					do_resize(&(disk->dev),npart,new_size.start,new_size.end);
				}
				if(strcmp(fs_name,"Without Filesystem"))
					do_mkfs(&(disk->dev),npart,fs_name);
		}

		if(key==NEWT_KEY_PGDN){
			new_size.end-=CIL2SEC(disk->dev,2);
			if(new_size.end<disk->geom->start) new_size.end=disk->geom->start;

			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.end));
			newtEntrySet(form->end_new,ptrb,1);
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.start));
			newtEntrySet(form->start_new,ptrb,1);
			sprintf(ptrb,"%d", (int)SEC2MB( (new_size.end- new_size.start)));
			newtEntrySet(form->tamano_new,ptrb,1);


		}if(key==NEWT_KEY_PGUP){
			new_size.end+=CIL2SEC(disk->dev,2);
			if(new_size.end>disk->geom->end) new_size.end=disk->geom->end;
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.end));
			newtEntrySet(form->end_new,ptrb,1);
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.start));
			newtEntrySet(form->start_new,ptrb,1);
			sprintf(ptrb,"%d", (int)SEC2MB( (new_size.end- new_size.start)));
			newtEntrySet(form->tamano_new,ptrb,1);

		}if(key==NEWT_KEY_LEFT){
			new_size.end-=50;
			if(new_size.end<disk->geom->start) new_size.end=disk->geom->start;
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.end));
			newtEntrySet(form->end_new,ptrb,1);
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.start));
			newtEntrySet(form->start_new,ptrb,1);
			sprintf(ptrb,"%d", (int)SEC2MB( (new_size.end- new_size.start)));
			newtEntrySet(form->tamano_new,ptrb,1);

		}if(key==NEWT_KEY_RIGHT){
			new_size.end+=50;
			if(new_size.end>disk->geom->end) new_size.end=disk->geom->end;
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.end));
			newtEntrySet(form->end_new,ptrb,1);
			sprintf(ptrb,"%d",(int)SEC2CIL(disk->dev,new_size.start));
			newtEntrySet(form->start_new,ptrb,1);
			sprintf(ptrb,"%d", (int)SEC2MB( (new_size.end- new_size.start)));
			newtEntrySet(form->tamano_new,ptrb,1);

		}
		newtScaleSet(form->progress,(100*new_size.end)/disk->geom->end );
	}while(key!=(int)'' && answer!=form->cancel  && answer!=form->ok);
	return 1;

}
int finalize_form(edit *form){
	newtFormDestroy(form->form);
	newtPopWindow();
	return 1;
}
int finalize_disk(disk_info *disk)
{
	ped_disk_close(disk->disk);
	return 1;
}
int edit_partition (PedDevice *dev,int npart)
{
	edit *form;
	disk_info *disk;

        form=(edit*)NP_malloc(sizeof(edit));
        disk=(disk_info*)NP_malloc(sizeof(disk_info));

	if(!init_disk_info(disk,dev,npart)){
		/* La excepci�n se lanza desde dentro de init_disk_info*/
		return 0;
	}

	sprintf(ptrb,_("Editing %s%d"),dev->path,npart);
	/* Cuando se da a init_form por segunda vez da fallo */
	init_form(form,ptrb);
	/* Get some information */
	run_form(form, disk,npart);
	finalize_form(form);
	finalize_disk(disk);
        free(form);
        free(disk);
        
	return 1;
}
