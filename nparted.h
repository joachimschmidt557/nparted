/*
    nparted - a frontend to libparted for manipulating disk partitions
    thougth to dbootstrap
    Copyright (C) 1998-2000 Mario Teijeiro Otero Esteve Fernández 
    Jaime Villate La Espiral
	
    	Mario Teijeiro Otero <asimovi@teleline.es>
	Esteve Fernández < esteve@crosswinds.net >
	Jaime Villate <villate@fe.up.pt>
	La Espiral  http://www.laespiral.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _NPARTED_
#define _NPARTED_

/* Para cuando tenga soporte gettex con configure*/
//#include "config.h"

#ifdef ENABLE_NLS
# define _(string) (gettext(string))
//# ifdef HAVE_LIBINTL_H
#  include <libintl.h>
//# endif
#else
# define _(string) (string)
#endif

#include <stdlib.h>
#include <parted/parted.h>
#include <newt.h>

#include "exception_manager.h"

typedef struct _size size;
struct _size{
	int width;
	int height;
};
typedef struct _strech strech;
struct _strech {
	PedSector start;
	PedSector end;
	struct _strech *next;
};

/* Global buffer */
extern char ptrb[256];	
extern size screen_size;
extern size mainwin_size;


/* Some functions utils */
void *NP_malloc (int size);
PedSector is_sector_valid(char *d);
char *get_name_device_type ( PedDeviceType type);
#define BETWEEN(A,B,C) ( ((A)>=(B))&& ((A)<=(C)) )
#define SEC2CIL(DEV,A) ( (A) / ( DEV->heads*DEV->sectors)+1 )
#define CIL2SEC(DEV,A) ( (A-1)*DEV->heads*DEV->sectors)
#define MB2SEC(A)	((A)*1024*1024/512)
#define SEC2MB(A)	((A)*512/(1024*1024))

/* functions with gui */
int edit_partition ( PedDevice *dev, int npart );
int copy_partition ( PedDevice *dev, int npart );
int del_partition( PedDevice *dev, int part);
int add_partition( PedDevice *dev);

char* select_file_system ( void );
char select_partition_type ( void );
/* Lib from programa parted */
int do_check (PedDevice** dev, int npart);
int do_cp (PedDevice** dev,int npart_src, char* dev_dest,
		int npart_dest);
int do_mklabel (PedDevice** dev,char *label);
int do_mkfs (PedDevice** dev,int npart, char *fs_type);
int do_mkpart (PedDevice** dev,char ptype,char *ftype,
		PedSector start,PedSector end);
int do_mkpartfs (PedDevice** dev,char ptype,char *ftype,
		PedSector start, PedSector end);
int do_move (PedDevice** dev,int part_src,PedSector start, PedSector end);
int do_name (PedDevice** dev, int npart,char *name);
int do_resize (PedDevice** dev,int npart, PedSector start, PedSector end);
int do_rm (PedDevice** dev, int npart);
int do_set (PedDevice** dev, int npart, char *flag_name,int state);
#endif
