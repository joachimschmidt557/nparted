/* Automatically generated by po2tbl.sed from nparted.pot.  */

#if HAVE_CONFIG_H
# include <config.h>
#endif

#include "libgettext.h"

const struct _msg_ent _msg_tbl[] = {
  {"", 1},
  {"Can't have two parts extended in one disk", 2},
  {"Adding a part to %s", 3},
  {"Growable", 4},
  {"Do fyle sistem", 5},
  {"Select partition type", 6},
  {"Select file system", 7},
  {"Create", 8},
  {"Cancel", 9},
  {"free strechs", 10},
  {"Start", 11},
  {" Cil.", 12},
  {"End", 13},
  {"Size", 14},
  {" Mb", 15},
  {"Primary", 16},
  {"Extend", 17},
  {"Logic", 18},
  {"a number is needed", 19},
  {"Out limits", 20},
  {"I can't do filesystem on a extended partition", 21},
  {"Are you sure create in %s a %c partition of type %s from %Ld to %Ld?", 22},
  {"Are you sure that you del %s%-2d partition?", 23},
  {"Tamano", 24},
  {"Now", 25},
  {"Max.", 26},
  {"New geom", 27},
  {"Grow too much posibble", 28},
  {"Without Filesystem", 29},
  {"Select", 30},
  {"OK", 31},
  {"CANCEL", 32},
  {"Device not valid", 33},
  {"Partition not valid", 34},
  {"I can't get de max partition's geometry", 35},
  {"Change the file system will destroy any data", 36},
  {"Are you soure?", 37},
  {"Editing %s%d", 38},
  {"You don't have seen this,report me", 39},
  {"Information message", 40},
  {"!!!----WARNING----!!!", 41},
  {"!!!----ERROR----!!!", 42},
  {"!!!----FATAL ERROR ----!!!", 43},
  {"You has found a bug, please report me", 44},
  {"Feature yet implemented", 45},
  {"FIX", 46},
  {"YES", 47},
  {"NO", 48},
  {"RETRY", 49},
  {"IGNORE", 50},
  {"Warning", 51},
  {"Device detected, but I can't open it", 52},
  {"No device detected, Are you root? ", 53},
  {"Select the disk or partition", 54},
  {" Add ", 55},
  {" Del ", 56},
  {" Edit", 57},
  {" Copy", 58},
  {" Quit", 59},
  {"Intern error have ocurred", 60},
  {"A Hard disk, can't edit, select a partition or add a new partition", 61},
  {"Can't del a hard disk, if you want del, remove it from equipment ;-)", 62},
  {"Please, select the partition to copy,I can't copy the whole disk", 63},
  {"Debian Instalation", 64},
  {"%s v.%s", 65},
  {"NParted ", 66},
  {"Partition doesn't exist.", 67},
  {"Source partition doesn't exist.", 68},
  {"Can't copy extended partitions.", 69},
  {"Destination partition doesn't exist.", 70},
  {"Destination partition is being used.", 71},
  {"Partition(s) on %s are being used.", 72},
  {"Partition is being used.", 73},
  {"Unknown filesystem type.", 74},
  {"Unknown file system type.", 75},
  {"Can't move extended partitions.", 76},
  {"Can't move a partition onto itself.  Try using resize, perhaps?", 77},
  {"I can't malloc, abort", 78},
  {"Please, select file system", 79},
  {"primary", 80},
  {"logical", 81},
  {"extended", 82},
  {"Please, select partition_type", 83},
};

int _msg_tbl_length = 83;
