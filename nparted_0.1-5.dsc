-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: nparted
Version: 0.1-5
Binary: nparted
Maintainer: Julien LEMOINE <speedblue@debian.org>
Architecture: any
Standards-Version: 3.5.2
Build-Depends: debhelper (>> 3.0.0), docbook-utils | docbook-to-man, libparted1.4-dev (>> 1.4.0), libnewt-dev (>> 0.50.0), uuid-dev (>= 1.2), gettext (>= 0.10)
Files: 
 1fe3554c380ca549ed5a4dd894d83234 105198 nparted_0.1.orig.tar.gz
 24515157d670dc7289b3e1d35ff84fa0 24099 nparted_0.1-5.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.6 (GNU/Linux)
Comment: For info see http://www.gnupg.org

iD8DBQE8nRfGc29c8N2YKnURAroLAKDUYQdQEe4QYjO383sy49jLCFQHVgCgoGRl
WFbVhoWdFAzyjd5GrjYTfMk=
=QKcJ
-----END PGP SIGNATURE-----
