/*
    nparted - a frontend to libparted for manipulating disk partitions
    thinked to dbootstrap
    Copyright (C) 1998-2000 Mario Teijeiro Otero Esteve Fern�ndez 
    Jaime Villate La Espiral
	
    	Mario Teijeiro Otero <asimovi@teleline.es>
	Esteve Fern�ndez < esteve@crosswinds.net >
	Jaime Villate <villate@fe.up.pt>
	La Espiral  http://www.laespiral.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


/* Consideraciones, es mejor abrir y cerrar todos los disks y devices
 * que se utilicen , no dejarlos abiertos y que se cierren cuando se 
 * salga. as� es m�s f�cil de que no se nos escape alg�na estructura
 * sin cerrar
 */
#include "nparted.h"

char ptrb[256];


size screen_size;
size mainwin_size;
typedef struct _itemlist itemlist;
struct _itemlist{
	char dev[20]; /*Nombre del dispositivo al cual pertenece */
	int part; /* N�mero de partici�n -1 si es un disco*/
};




void done_parted(void);



/* crea una lista de componentes con data un itemlist*/
int do_list_disks( newtComponent list )
{
	PedDevice *dev;
	PedPartition *part;
	PedDisk *disk;
	PedGeometry *geom;
	PedFileSystemType *fs_type;
	itemlist *item;
	char fs_name[40];
	int i;
	long long tam,start,end;
	dev=NULL;

	for (i=0;(dev=ped_device_get_next(dev))!=NULL;i++){
		tam=(dev->sector_size*dev->length)/(1024*1024);
		sprintf(ptrb,"%s: %s-%s(%d-%d-%d %dB/sec) %LdMb",dev->path,
				get_name_device_type(dev->type),
				dev->model,dev->heads,dev->sectors,
				dev->cylinders, dev->sector_size, tam);

		item=(itemlist*)NP_malloc(sizeof(itemlist));
		strcpy(item->dev,dev->path);
		item->dev[8]='\0';
		item->part=-1;
		
		newtListboxAppendEntry(list,ptrb,item);

		disk=ped_disk_open(dev);
		part=NULL;
		if (disk==NULL){
			if(ped_exception_throw(PED_EXCEPTION_FATAL,
					PED_EXCEPTION_IGNORE+
					PED_EXCEPTION_CANCEL,
					_("Device detected, but I can't open it"))==
					PED_EXCEPTION_CANCEL){
				done_parted();
				exit (-1);
			}else{
				continue;
			}
		}
		while((part=ped_disk_next_partition(disk,part))!=NULL){
			if(part->num==-1){
				if (part->type!=PED_PARTITION_FREESPACE)
					continue;	
			}
			geom=&(part->geom);
			fs_type=ped_file_system_probe(geom);
			if (fs_type!=NULL && fs_type->name!=NULL)
				strcpy(fs_name,fs_type->name);
			else
				strcpy(fs_name,"");
			tam=geom->length*dev->sector_size; //bytes
			tam=tam/(1024*1024); //Mbytes
			/* Tam-cilindro=head*sectors*sizeofsector*/
			start=geom->start/(dev->heads*dev->sectors)+1;
			end=geom->end/(dev->heads*dev->sectors)+1;


			sprintf(ptrb,"     %s%-2d: %-8s Cil:(%Ld-%Ld) %LdMB %-8s",
					dev->path, part->num, 
					ped_partition_type_get_name(part->type),
					start,end,tam,fs_name);

			item=(itemlist*)NP_malloc(sizeof(itemlist));
			strcpy(item->dev,dev->path);
			item->dev[8]='\0';
			item->part=part->num;


			newtListboxAppendEntry(list,ptrb,item);

		}
		ped_disk_close(disk);

	}
	if (i==0){
		/*Ning�n disco duro encontrado, Mejor lanzamos una
		excepci�n diciendo que se no se encontr� ning�n 
		disco y que posiblemente sea porque no es superusuario
		*/
		ped_exception_throw(PED_EXCEPTION_FATAL,
					PED_EXCEPTION_OK,
					_("No device detected, Are you root? "));

		//	done_parted(); /* Pero esto que co�o hace aqu�, 
		//		  deben ser restos de primeras pruebas */
		//exit(0);
		return 0;
	}
	return 1;
}

/* Esta funci�n debe volver cuando hay que salir del programa */
void do_main_window()
{
	newtComponent formMain,list_disks,add,del,edit,copy,quit,ans;
	struct newtExitStruct toexit;
	itemlist *item;
	int key;
	mainwin_size.width=screen_size.width-5;
	mainwin_size.height=screen_size.height-5;
	
	newtCenteredWindow(mainwin_size.width, mainwin_size.height,
			_("Select the disk or partition"));	

	formMain=newtForm(NULL,NULL,0);
	list_disks=newtListbox(1,2,13,NEWT_FLAG_SCROLL);
	newtListboxSetWidth(list_disks,62);
	if (!do_list_disks(list_disks)) return; 

	add=newtButton (mainwin_size.width-10,2,_(" Add "));
	del=newtButton(mainwin_size.width-10,7,_(" Del "));
	edit=newtButton(mainwin_size.width-10,12,_(" Edit"));
	copy=newtButton(5,16,_(" Copy"));
	quit=newtButton(mainwin_size.width-30,16,_(" Quit"));

	newtFormAddComponents(formMain,add,del,edit,copy,quit,list_disks,
		NULL);
	ans=NULL;

	do{
		newtFormRun(formMain,&toexit);
		key=-1;
		if (toexit.reason==NEWT_EXIT_COMPONENT)
			ans=toexit.u.co;
		else
			key=toexit.u.key;
		if(ans==edit){
			item=(itemlist*)newtListboxGetCurrent(list_disks);	
			if((!item)&& (item->dev[0]!='/')){
				ped_exception_throw(PED_EXCEPTION_FATAL,
						PED_EXCEPTION_OK,
						_("Intern error have ocurred"));
				done_parted();
				exit(-1);
			}
			if (item->part==-1){
				ped_exception_throw(PED_EXCEPTION_INFORMATION,
						PED_EXCEPTION_OK,
						_("A Hard disk, can't edit, select a partition or add a new partition"));
				ped_exception_catch();

			}else{

				edit_partition(ped_device_get(item->dev), 
						item->part);	
			}
		}else
		if (ans==add){
			item=(itemlist*) newtListboxGetCurrent(list_disks);
			add_partition(ped_device_get(item->dev));
		}else					
		if (ans==del){
			item=(itemlist*) newtListboxGetCurrent(list_disks);
			if(item->part==-1){
				ped_exception_throw(PED_EXCEPTION_INFORMATION,
						PED_EXCEPTION_OK,
						_("Can't del a hard disk, if you want del, remove it from equipment ;-)"));
				ped_exception_catch();
			}else{
				del_partition(ped_device_get(item->dev),
						item->part);
			}
		}else
		if (ans==copy){
			item=(itemlist*) newtListboxGetCurrent(list_disks);
			if (item->part==-1){
				ped_exception_throw(PED_EXCEPTION_INFORMATION,
						PED_EXCEPTION_OK,
						_("Please, select the partition to copy,I can't copy the whole disk"));
				ped_exception_catch();
			}else{
				copy_partition(ped_device_get(item->dev),
						item->part);
			}
		}
		
		/* Despu�s de cada operaci�n volvemos a leer la lista
		 * de particiones, as� que tenemos que actualizar list_disks
		 */
		if(ans!=quit){
			/*FIXME:Borramos todas las entradas, quizas no libere el
			 * data de la lista, pero es poca memoria, quizas hay 
			 * que eliminar entrada por entrada*/
			newtListboxClear(list_disks)
			/* Volvemos a leer la lista*/;		
			do_list_disks( list_disks );
		}
	}while(key!=NEWT_KEY_F1&&ans!=quit);
	newtFormDestroy(formMain);
}
void init_parted (void )
{
	ped_exception_set_handler(exception_handler);
	ped_init();
	ped_device_probe_all();

	newtInit();
	newtCls();

	newtDrawRootText(0,0,_("Debian Instalation"));
#if defined(VERSION) && defined(PACKAGE)
	sprintf(ptrb,_("%s v.%s"),PACKAGE,VERSION);
#else
	sprintf(ptrb,_("NParted "));
#endif
	newtDrawRootText(-25,-1,ptrb);

	newtGetScreenSize(&screen_size.width, &screen_size.height);

}
void done_parted(void )
{
	newtFinished();
	ped_done();
}
#ifndef BOOTFLOPPIES
int main (void)
#else
int init_nparted(void)
#endif
{
	init_parted();

	do_main_window();

	done_parted();
	return 0;
}

