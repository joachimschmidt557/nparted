/*
    nparted - a frontend to libparted for manipulating disk partitions
    thinked to dbootstrap
    Copyright (C) 1998-2000 Mario Teijeiro Otero Esteve Fern�ndez 
    Jaime Villate La Espiral
	
    	Mario Teijeiro Otero <asimovi@teleline.es>
	Esteve Fern�ndez < esteve@crosswinds.net >
	Jaime Villate <villate@fe.up.pt>
	La Espiral  http://www.laespiral.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <newt.h>
#include "nparted.h"
#include "exception_manager.h"

#undef _(A)
#define _(A) (A)
static char *names_exception_types[]={
	_("You don't have seen this,report me"),
	_("Information message"),
	_("!!!----WARNING----!!!"),
	_("!!!----ERROR----!!!"),
	_("!!!----FATAL ERROR ----!!!"),
	_("You has found a bug, please report me"),
	_("Feature yet implemented"),
};
static button_option options[]={
	{1,   	{_("FIX"),} ,			{PED_EXCEPTION_FIX,},{}			},
	{1,	{_("YES"),},			{PED_EXCEPTION_YES,},{}				},
	{1,	{_("NO"),},			{PED_EXCEPTION_NO,},{}				},
	{1,	{_("OK"),},			{PED_EXCEPTION_OK,},{}				},
	{1,	{_("RETRY"),},			{PED_EXCEPTION_RETRY,},{}				},
	{1,	{_("IGNORE"),},			{PED_EXCEPTION_IGNORE,},{}				},
	{1,	{_("CANCEL"),},			{PED_EXCEPTION_CANCEL,},{}				},
	{2,	{_("OK"),_("CANCEL"),},		{PED_EXCEPTION_OK,PED_EXCEPTION_CANCEL,},{}	},
	{2,	{_("YES"),_("NO"),},		{PED_EXCEPTION_YES,PED_EXCEPTION_NO,},{}		},
	{3,	{_("YES"),_("NO"),_("CANCEL"),},{PED_EXCEPTION_YES,PED_EXCEPTION_NO,PED_EXCEPTION_CANCEL,},{}},
	{2,	{_("IGNORE"),_("CANCEL"),},	{PED_EXCEPTION_IGNORE,PED_EXCEPTION_CANCEL,},{}	},
	{2,	{_("RETRY"),_("CANCEL"),},	{PED_EXCEPTION_RETRY,PED_EXCEPTION_CANCEL,},{}	},
	{3,{_("RETRY"),_("IGNORE"),_("CANCEL"),},{PED_EXCEPTION_RETRY,PED_EXCEPTION_IGNORE,PED_EXCEPTION_CANCEL,},{}}
};


/*PedExceptionOption*/int do_message_window(const char *title, char* message, button_option *list_buttons)
{
	newtComponent form,text,button;
	int i,x,y, size_button;
	
	if (message==NULL){
		text=newtTextboxReflowed(1,1,"Has encontrado un bug",54,5,5,0);
	}else{
		text=newtTextboxReflowed(1,1,message,54,5,5,0);
	}
	if (title==NULL){
		newtCenteredWindow(60,newtTextboxGetNumLines(text)+7,_("Warning"));
	}else{
		newtCenteredWindow(60,newtTextboxGetNumLines(text)+7,title);
	}

	form=newtForm(NULL, NULL, 0);
	newtFormAddComponent(form,text);

	x=60/(list_buttons->num+1); /*separacion entre botones*/
	y=newtTextboxGetNumLines(text)+2;
	if (list_buttons==NULL) list_buttons=&options[3];
	for (i=0; i<list_buttons->num; i++){
		size_button=strlen(list_buttons->label[i])+5;
		list_buttons->button[i]= newtButton(x*(i+1)-size_button/2,y,
				list_buttons->label[i]);
		newtFormAddComponent(form,list_buttons->button[i]);
	}
	button=newtRunForm(form);

	for ( i =0; i <list_buttons->num; i++){
		if (list_buttons->button[i]==button)
			break;
	}
	/*En button tenemos el boton que hizo que sali�semos de la 
	 * pantalla*/

	newtFormDestroy(form);
	newtPopWindow();
	if (i<list_buttons->num)
		return list_buttons->op2ret[i];
	else
		return PED_EXCEPTION_UNHANDLED;
}

/* Cuando Ped lanza una excepci�n, �sta puede ser de varios tipos
 * PED_EXCEPTION_NO_FEATURE
 * PED_EXCEPTION_INFORMATION -> info general
 * PED_EXCEPTION_WARNING
 * PED_EXCEPTION_ERROR
 * PED_EXCEPTION_FATAL
 * PED_EXCEPTION_BUG
 * y �sta excepti�n puede esperar los siguientes tipos de respuesta:
 * UNHANDLED -> significa que la excepci�n on fue manejada
 * FIX ->	algo que todav�a hay que implementar
 * YES ->	
 * NO ->
 * OK ->
 * RETRY
 * IGNORE
 * CANCEL
 * todos estas opciones est�n en exception como una � l�gica.
 * As� solo tenemos que mostrar el mensaje y una cantidad de respuestas
 * a esperar por el usuario.
 */

PedExceptionOption exception_handler( PedException* exception)   
{
	button_option *list_button;

	switch (exception->options){
		case PED_EXCEPTION_FIX:
			list_button=&options[0];break;
		case PED_EXCEPTION_YES:
			list_button=&options[1];break;
		case PED_EXCEPTION_NO:
			list_button=&options[2];break;
		case PED_EXCEPTION_OK:
			list_button=&options[3];break;
		case PED_EXCEPTION_RETRY:
			list_button=&options[4];break;
		case PED_EXCEPTION_IGNORE:
			list_button=&options[5];break;
		case PED_EXCEPTION_CANCEL:  
			list_button=&options[6];break;
		case PED_EXCEPTION_OK_CANCEL:
			list_button=&options[7];break;
		case PED_EXCEPTION_YES_NO:
			list_button=&options[8];break;
		case PED_EXCEPTION_YES_NO_CANCEL: 
			list_button=&options[9];break;
		case PED_EXCEPTION_IGNORE_CANCEL: 
			list_button=&options[10];break;
		case PED_EXCEPTION_RETRY_CANCEL: 
			list_button=&options[11];break;
		case PED_EXCEPTION_RETRY_IGNORE_CANCEL: 
			list_button=&options[12];break;
	default:
		list_button=&options[3];break;
	}
	return do_message_window(names_exception_types[exception->type],
			exception->message,list_button);
}
